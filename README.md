# custom-twitter

Personalización de la pantalla principal de Twitter para evitar las tendencias. 
Reemplaza la sección explorar por un buscador simple y cambia el título a la sección por "Buscar".

Diseñado exclusivamente para **Firefox**.

![captura demostrativa](img/Captura.png)

## Aplicando el tema:

1. Configuración
* Abrir una nueva pestaña en Firefox con la siguiente dirección: `about:config`.
* Escribir en el navegador: `toolkit.legacyUserProfileCustomizations.stylesheets`.
* Doble click para cambiar su valor a `true`.
2. Instalación
* Abrir una nueva pestaña en Firefox con la siguiente dirección: `about:support`.
* Buscar `Carpeta del perfil` y pulsar sobre el botón `Abrir carpeta`.
* Crear una carpeta "chrome" y copiar el fichero `userContent.css`.
3. Aplicando el tema
* Seleccionar en `más opciones`: `pantalla` > `noche clara`.

